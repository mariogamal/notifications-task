//
//  Alert.swift
//  task
//
//  Created by Mario Gamal on 2/19/20.
//  Copyright © 2020 Mario Gamal. All rights reserved.
//

import Foundation
struct Alert {
    var id: String
    var alert: String
    var day: String
    var hour: String
}
