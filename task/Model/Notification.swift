//
//  struct Notification {     var id-String     var title-String     var datetime-DateComponents }.swift
//  task
//
//  Created by Mario Gamal on 2/19/20.
//  Copyright © 2020 Mario Gamal. All rights reserved.
//

import Foundation

struct Notification {
    var id: String
    var title: String
    var datetime: DateComponents
}
