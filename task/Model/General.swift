//
//  General.swift
//  Masafah
//
//  Created by Mario Gamal on 6/25/19.
//  Copyright © 2019 Vavisa IT Solutions. All rights reserved.
//

struct GeneralResponse: Codable {
    var message: String?
    var error: String?
}

struct GenericResponse<T: Codable>: Codable {
    var model: T
    var message: String
    
    private enum CodingKeys: String, CodingKey {
        case model
        case message
        
        var localizedText: String {
            switch self {
            case .model: return String(describing: T.self)
            case .message: return "message"
            }
        }
    }
}
