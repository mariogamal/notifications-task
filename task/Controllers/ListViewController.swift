//
//  ViewController.swift
//  task
//
//  Created by Mario Gamal on 2/19/20.
//  Copyright © 2020 Mario Gamal. All rights reserved.
//

import UIKit

class ListViewController: UITableViewController,  XMLParserDelegate{

    let recordKey = "notification"
    let dictionaryKeys = Set<String>(["id", "alert", "day", "hour"])

    var results: [[String: String]]?
    var currentDictionary: [String: String]?
    var currentValue: String?
    
    var alerts = [Alert]()
    var selectedAlert = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let logoutBarButtonItem = UIBarButtonItem(title: "Cancel All", style: .done, target: self, action: #selector(cancelAll))
        self.navigationItem.rightBarButtonItem  = logoutBarButtonItem
        getAlerts()
    }

    @objc func cancelAll(){
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        showAlert(message: "Canceled Successfully")
    }

    func getAlerts() {
        let request = URLRequest(url: URL(string: Constants.URL)!)

        DispatchQueue.main.async {
            LoadingController.shared.show()
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in

            DispatchQueue.main.async {
                LoadingController.shared.hide()
            }
            
            guard let data = data, error == nil else {
                print(error ?? "Unknown error")
                return
            }

            let parser = XMLParser(data: data)
            parser.delegate = self
            if parser.parse() {
                for result in self.results! {
                    let alert = Alert(id: result["id"]!, alert: result["alert"]!, day: result["day"]!, hour: result["hour"]!)
                    self.alerts.append(alert)
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
        task.resume()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return alerts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath)
        cell.textLabel?.text = alerts[indexPath.row].alert
        cell.selectionStyle = .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedAlert = indexPath.row
        performSegue(withIdentifier: "AlertActions", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AlertActions" {
            if let destVC = segue.destination as? DetailsViewController {
               destVC.alert = alerts[selectedAlert]
            }
        }
    }
    
    func showAlert(message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
        self.present(alertController, animated: true, completion: nil)
    }
}

extension ListViewController {

    func parserDidStartDocument(_ parser: XMLParser) {
        results = []
    }

    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        if elementName == recordKey {
            currentDictionary = [:]
        } else if dictionaryKeys.contains(elementName) {
            currentValue = ""
        }
    }

    func parser(_ parser: XMLParser, foundCharacters string: String) {
        currentValue? += string
    }

    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == recordKey {
            results!.append(currentDictionary!)
            currentDictionary = nil
        } else if dictionaryKeys.contains(elementName) {
            currentDictionary![elementName] = currentValue
            currentValue = nil
        }
    }

    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print(parseError)

        currentValue = nil
        currentDictionary = nil
        results = nil
    }

}
