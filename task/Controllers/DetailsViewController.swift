//
//  DetailsViewController.swift
//  task
//
//  Created by Mario Gamal on 2/19/20.
//  Copyright © 2020 Mario Gamal. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    var alert: Alert?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var scheduleNotificationSwitch: UISwitch!
    @IBOutlet weak var repeatNotificationSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = alert?.alert
        checkScheduledNotifications()
    }
    
    @IBAction func scheduleClicked(_ sender: Any) {
        cancelNotification()
        if scheduleNotificationSwitch.isOn {
            schedule()
        } else {
            repeatNotificationSwitch.isOn = false
        }
    }
    
    @IBAction func repeatClicked(_ sender: Any) {
        cancelNotification()
        
        if repeatNotificationSwitch.isOn {
            scheduleNotificationSwitch.isOn = true
            schedule()
        } else if !repeatNotificationSwitch.isOn
            && scheduleNotificationSwitch.isOn {
            schedule()
        }
    }
    
    func createDate() -> Date{

        var components = DateComponents()
        components.hour = Int(alert!.hour)
        components.minute = 0
        components.year = getYear()
        components.weekday = getWeekDay(day: alert!.day)
        components.weekdayOrdinal = 10
        components.timeZone = .current

        let calendar = Calendar(identifier: .gregorian)
        return calendar.date(from: components)!
    }
    
    func getWeekDay(day: String) -> Int {
        switch day.lowercased() {
        case "sunday":
            return 1
        case "monday":
            return 2
        case "tuesday":
            return 3
        case "wednesday":
            return 4
        case "thursday":
            return 5
        case "friday":
            return 6
        case "saturday":
            return 7
        default:
            return 0
        }
    }

    func getYear() -> Int {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        let yearString = dateFormatter.string(from: date)
        return Int(yearString)!
    }
    func scheduleNotification() {

        let triggerWeekly = Calendar.current.dateComponents([.weekday,.hour,.minute,.second,], from: createDate())

        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerWeekly, repeats: repeatNotificationSwitch.isOn)

        let content = UNMutableNotificationContent()
        content.title = alert!.alert
        content.sound = UNNotificationSound.default
        content.categoryIdentifier = "WaveLine Task"

        let request = UNNotificationRequest(identifier: alert!.id, content: content, trigger: trigger)

        UNUserNotificationCenter.current().add(request) {(error) in
            if let error = error {
                print("We had an error: \(error)")
            }
        }
    }

    private func requestAuthorization(){
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { granted, error in

            if granted == true && error == nil {
                self.scheduleNotification()
            }
        }
    }
    
    func schedule(){
        UNUserNotificationCenter.current().getNotificationSettings { settings in

            switch settings.authorizationStatus {
            case .notDetermined:
                self.requestAuthorization()
            case .authorized, .provisional:
                self.scheduleNotification()
            default:
                break
            }
        }
    }
    
    func cancelNotification() {
        let center = UNUserNotificationCenter.current()
        center.removeDeliveredNotifications(withIdentifiers: [alert!.id])
        center.removePendingNotificationRequests(withIdentifiers: [alert!.id])
    }
    
    func checkScheduledNotifications(){
        UNUserNotificationCenter.current().getPendingNotificationRequests { notifications in

            for notification in notifications {
                if notification.identifier == self.alert?.id {
                    self.scheduleNotificationSwitch.isOn = true
                    if notification.trigger!.repeats {
                        self.repeatNotificationSwitch.isOn = true
                    }
                }
            }
        }
    }
}
