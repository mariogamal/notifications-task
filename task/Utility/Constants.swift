//
//  Constants.swift
//  Masafah
//
//  Created by Mario Gamal on 6/24/19.
//  Copyright © 2019 Vavisa IT Solutions. All rights reserved.
//

import Foundation

public class Constants {
    public static let URL = "https://yabila.com/localalerts.php"
    public static let ACCESS_TOKEN_KEY = "access_token"
    public static let USER_LOGGED = "user_logged"
    public static let MOBILE = "mobile"
    public static let COUNTRY_ID = "country_id"
    public static let LANGUAGE_CODE = "language_code"
    public static let ONE_SIGNAL_ID = "02c9f53c-ad9a-4de0-bc2e-56a420dce18e"
}
