//
//  LocalNotificationManager.swift
//  task
//
//  Created by Mario Gamal on 2/19/20.
//  Copyright © 2020 Mario Gamal. All rights reserved.
//

import UIKit

class LocalNotificationManager {

    var alert: Alert?
    
    func listScheduledNotifications()
    {
        UNUserNotificationCenter.current().getPendingNotificationRequests { notifications in

            for notification in notifications {
                print(notification)
            }
        }
    }
    
    private func requestAuthorization()
    {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { granted, error in

            if granted == true && error == nil {
                self.scheduleNotification()
            }
        }
    }
    
    func schedule(alert: Alert)
    {
        self.alert = alert
        UNUserNotificationCenter.current().getNotificationSettings { settings in

            switch settings.authorizationStatus {
            case .notDetermined:
                self.requestAuthorization()
            case .authorized, .provisional:
                self.scheduleNotification()
            default:
                break

            }
        }
    }
    
    private func scheduleNotification()
    {
        
//            let content      = UNMutableNotificationContent()
//        content.title    = alert!.alert
//            content.sound    = .default
//
//            let trigger = UNCalendarNotificationTrigger(dateMatching: .datetime, repeats: false)
//
//            let request = UNNotificationRequest(identifier: alert.id, content: content, trigger: trigger)
//
//            UNUserNotificationCenter.current().add(request) { error in
//
//                guard error == nil else { return }
//
//                print("Notification scheduled! --- ID = \(notification.id)")
//            }
        
    }
}
